SHELL=/bin/bash
MINIFY=$(shell if which gominify &>/dev/null; then echo "gominify";else echo "minify";fi)
SASS=$(shell if which sassc &>/dev/null; then echo "sassc";else echo "sass";fi)

public: build
build: clean conditionalClone cachedBuild compress
.PHONY: public static
cachedBuild: conditionalClone prepare css html static
html: conditionalClone
	@make --no-print-directory BUILD_LANG=en FASTLANE_LANG=en-US _buildSite
	@make --no-print-directory BUILD_LANG=no FASTLANE_LANG=no-NO _buildSite
	cp _redirects public/
	cd public && ln -s en/*png .
_buildSite: OUT_FILE=public/$(BUILD_LANG)/index.html
_buildSite:
	@[ "$(BUILD_LANG)" != "" ]
	mkdir -p public/$(BUILD_LANG)
	cat head.html > $(OUT_FILE)
	cat strings/$(BUILD_LANG)/menu.html >> $(OUT_FILE)
	echo '<section>' >> $(OUT_FILE)
	cat org.zerodogg.migraineLog/android/fastlane/metadata/android/$(FASTLANE_LANG)/full_description.txt |perl -p -E 's/\n/<br \/>/g' >> $(OUT_FILE)
	echo '</section>' >> $(OUT_FILE)
	cat strings/$(BUILD_LANG)/screenshots.html >> public/$(BUILD_LANG)/index.html
	cp org.zerodogg.migraineLog/android/fastlane/metadata/android/$(FASTLANE_LANG)/images/phoneScreenshots/*png public/$(BUILD_LANG)
	for section in freesoftware privacy localization issues install support; do\
		echo "<section id=\"$$section\">" >> $(OUT_FILE); \
		cat strings/$(BUILD_LANG)/$$section.html >> $(OUT_FILE); \
		echo '</section>' >> $(OUT_FILE); \
	done
	echo '<footer>' >> $(OUT_FILE)
	cat strings/$(BUILD_LANG)/footer.html >> $(OUT_FILE)
	echo '</footer>' >> $(OUT_FILE)
	echo '</main><script data-goatcounter="https://migrainelog.goatcounter.com/count" async src="//gc.zgo.at/count.v2.js" crossorigin="anonymous" integrity="sha384-PeYXrhTyEaBBz91ANMgpSbfN1kjioQNPHNDbMvevUVLJoWrVEjDCpKb71TehNAlj"></script></body></html>' >> public/$(BUILD_LANG)/index.html
	perl -pi -E 's/\{\{\{VERSION\}\}\}/$(shell cd org.zerodogg.migraineLog; git tag -l --sort=-v:refname|egrep '^v'|head -n1|perl -p -E 's/^v//')/g; s/\{\{\{YEAR\}\}\}/$(shell date '+%Y')/g; s/\{\{\{APPNAME\}\}\}/$(shell cat org.zerodogg.migraineLog/android/fastlane/metadata/android/$(FASTLANE_LANG)/title.txt)/g; s/\{\{\{DESCRIPTION\}\}\}/$(shell cat org.zerodogg.migraineLog/android/fastlane/metadata/android/$(FASTLANE_LANG)/short_description.txt)/g' public/$(BUILD_LANG)/index.html
	perl -pi -E 's{<html>}{<html lang="$(BUILD_LANG)">}' public/$(BUILD_LANG)/index.html
static: public/play-badge.png public/play-badge-no.png public/fdroid-badge.png public/fdroid-badge-no.png public/buymeacoffee.png public/icon.png public/appstore-badge.png public/appstore-badge-no.png
public/icon.png:
	wget -O public/icon.png "https://gitlab.com/mglog/org.zerodogg.migraineLog/-/raw/79a4810fff1420f74bde8bb5831ecfc936eae82d/android/app/src/main/res/mipmap-xxxhdpi/migrainelog.png?inline=false" || wget -O public/icon.png https://migrainelog.zerodogg.org/icon.png
public/play-badge.png:
	wget -O public/play-badge.png https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png || wget -O public/play-badge.png https://migrainelog.zerodogg.org/play-badge.png
public/play-badge-no.png:
	wget -O public/play-badge-no.png https://play.google.com/intl/en_us/badges/static/images/badges/no_badge_web_generic.png || wget -O public/play-badge-no.png https://migrainelog.zerodogg.org/play-badge-no.png
public/fdroid-badge.png:
	wget -O public/fdroid-badge.png 'https://gitlab.com/fdroid/artwork/-/raw/master/badge/get-it-on-en-gb.png?inline=false' || wget -O public/fdroid-badge.png https://migrainelog.zerodogg.org/fdroid-badge.png
public/fdroid-badge-no.png:
	wget -O public/fdroid-badge-no.png 'https://gitlab.com/zerodogg/artwork/-/raw/master/badge/get-it-on-nn.png?inline=false' || wget -O public/fdroid-badge-no.png https://migrainelog.zerodogg.org/fdroid-badge-no.png
public/appstore-badge.png:
	wget -O public/appstore-badge.png 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/640px-Download_on_the_App_Store_Badge.svg.png' || wget -O public/appstore-badge.png https://migrainelog.zerodogg.org/appstore-badge.png
public/appstore-badge-no.png:
	wget -O public/appstore-badge-no.png 'https://www.helsenorge.no/4ad5c4/globalassets/mobilapp/badges/apple-app-store-badge.png' || wget -O public/appstore-badge-no.png https://migrainelog.zerodogg.org/appstore-badge-no.png
public/buymeacoffee.png:
	wget -O public/buymeacoffee.png 'https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png' || wget -O public/buymeacoffee.png https://migrainelog.zerodogg.org/buymeacoffee.png
css:
	$(SASS) ./style.scss ./public/style.css
prepare:
	mkdir -p public
clean:
	rm -rf public
distclean: clean
	rm -rf org.zerodogg.migraineLog
clone: clean
	git clone https://gitlab.com/mglog/org.zerodogg.migraineLog.git
	@make --no-print-directory checkoutClone
conditionalClone:
	@if [ -d org.zerodogg.migraineLog ]; then\
		make --no-print-directory cloneUpdate; \
	else \
		make --no-print-directory clone;  \
	fi
cloneUpdate:
	cd org.zerodogg.migraineLog; git fetch --all;
	@make --no-print-directory checkoutClone
checkoutClone:
	cd org.zerodogg.migraineLog; VER="$$(git tag -l --sort=-v:refname|egrep '^v'|head -n1)"; git checkout main &>/dev/null; git branch -D "$$VER" &>/dev/null; git checkout -f -b "$$VER" "$$VER"
compress:
	for ftype in html css; do\
		for file in $$(find public \( -name "*.$$ftype" \)); do \
			mv "$$file" "$$file.tmp";\
			cat "$$file.tmp"|$(MINIFY) --html-keep-document-tags --type "$$ftype" -o "$$file";\
			rm -f "$$file.tmp";\
		done; \
	done
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' -o -name '*.txt' -o -name '*.xml' \) -print0 | xargs -0 gzip -9 -k
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' -o -name '*.txt' -o -name '*.xml' \) -print0 | xargs -0 brotli -k
onChange:
	while :;do echo ""; make --no-print-directory cachedBuild;sleep 0.3s;inotifywait -qq -e close,move,create . ;done
